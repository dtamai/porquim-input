CREATE TABLE assets (
  id SERIAL PRIMARY KEY,
  display_name VARCHAR(100) NOT NULL,
  asset_class_id SERIAL REFERENCES asset_classes NOT NULL,
  financial_institution_id SERIAL REFERENCES financial_institutions NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE UNIQUE INDEX idx_display_name_on_assets ON assets (lower(display_name));

SELECT diesel_manage_updated_at('assets');
