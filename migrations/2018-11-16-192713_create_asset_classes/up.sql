CREATE TABLE asset_classes (
  id SERIAL PRIMARY KEY,
  display_name VARCHAR(100) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE UNIQUE INDEX idx_display_name_on_asset_classes ON asset_classes (lower(display_name));

SELECT diesel_manage_updated_at('asset_classes');
