CREATE TABLE financial_institutions (
  id SERIAL PRIMARY KEY,
  display_name VARCHAR(100) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE UNIQUE INDEX idx_display_name_on_financial_institutions ON financial_institutions (lower(display_name));

SELECT diesel_manage_updated_at('financial_institutions');
