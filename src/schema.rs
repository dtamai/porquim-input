table! {
    asset_classes (id) {
        id -> Int4,
        display_name -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    assets (id) {
        id -> Int4,
        display_name -> Varchar,
        asset_class_id -> Int4,
        financial_institution_id -> Int4,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    financial_institutions (id) {
        id -> Int4,
        display_name -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(assets -> asset_classes (asset_class_id));
joinable!(assets -> financial_institutions (financial_institution_id));

allow_tables_to_appear_in_same_query!(
    asset_classes,
    assets,
    financial_institutions,
);
