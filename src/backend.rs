use crate::asset::*;
use crate::asset_class::*;
use crate::db;
use crate::financial_institution::*;
use crate::models::Error;

use std::sync::mpsc::*;
use std::sync::Mutex;

pub enum SendEvent {
    Ping,
    AssetClassCreate {
        display_name: String,
    },
    FinancialInstitutionCreate {
        display_name: String,
    },
    AssetCreate {
        display_name: String,
        asset_class_id: i32,
        financial_institution_id: i32,
    },
    AssetClassLoad,
    FinancialInstitutionLoad,
    AssetLoad,
}

pub enum ReceiveEvent {
    Pong,
    Message(String),
    ErrorMessage(String),
    AssetClassLoaded(Vec<AssetClass>),
    FinancialInsttutionLoaded(Vec<FinancialInstitution>),
    AssetLoaded(Vec<Asset>),
}

pub struct Backend {
    db_connection: db::PgConnection,
    receiver: Receiver<SendEvent>,
    response_channel: Sender<ReceiveEvent>,
}

type Result = std::result::Result<ReceiveEvent, Error>;

impl Backend {
    pub fn new(receiver: Receiver<SendEvent>, response_channel: Sender<ReceiveEvent>) -> Backend {
        let db_connection = db::connect_now().unwrap_or_else(|e| {
            let err = format!("DB connection failed with {}", e);
            response_channel
                .send(ReceiveEvent::Message(err.clone()))
                .unwrap();
            panic!(err.clone());
        });

        Backend {
            db_connection,
            receiver,
            response_channel,
        }
    }

    pub fn handle_events(self) {
        for evt in self.receiver.iter() {
            let result = match evt {
                SendEvent::Ping => Ok(ReceiveEvent::Pong),
                SendEvent::AssetClassCreate { display_name } => {
                    self.create_asset_class(display_name)
                }
                SendEvent::AssetClassLoad => self.load_asset_classes(),
                SendEvent::FinancialInstitutionCreate { display_name } => {
                    self.create_financial_institution(display_name)
                }
                SendEvent::FinancialInstitutionLoad => self.load_financial_institutions(),
                SendEvent::AssetCreate {
                    display_name,
                    asset_class_id,
                    financial_institution_id,
                } => self.create_asset(display_name, asset_class_id, financial_institution_id),
                SendEvent::AssetLoad => self.load_assets(),
            };

            match result {
                Ok(evt) => self.send_event(evt),
                Err(error) => self.send_event(ReceiveEvent::ErrorMessage(error.to_string())),
            }
        }
    }

    fn create_asset_class(&self, display_name: String) -> Result {
        let insertable = crate::asset_class::NewAssetClass {
            display_name: &display_name,
        };
        let result = crate::asset_class::ops::create(&self.db_connection, &insertable)?;

        match result {
            Some(asset_class) => Ok(ReceiveEvent::Message(format!(
                "Created asset class: '{}'",
                asset_class.display_name
            ))),
            None => Err(Error::AlreadyExists(display_name)),
        }
    }

    fn load_asset_classes(&self) -> Result {
        let result = crate::asset_class::ops::load_all(&self.db_connection)?;

        Ok(ReceiveEvent::AssetClassLoaded(result))
    }

    fn create_financial_institution(&self, display_name: String) -> Result {
        let insertable = crate::financial_institution::NewFinancialInstitution {
            display_name: &display_name,
        };
        let result = crate::financial_institution::ops::create(&self.db_connection, &insertable)?;

        match result {
            Some(financial_institution) => Ok(ReceiveEvent::Message(format!(
                "Created financial institution: '{}'",
                financial_institution.display_name
            ))),
            None => Err(Error::AlreadyExists(display_name)),
        }
    }

    fn load_financial_institutions(&self) -> Result {
        let result = crate::financial_institution::ops::load_all(&self.db_connection)?;

        Ok(ReceiveEvent::FinancialInsttutionLoaded(result))
    }

    fn create_asset(
        &self,
        display_name: String,
        asset_class_id: i32,
        financial_institution_id: i32,
    ) -> Result {
        let insertable = NewAsset {
            display_name: display_name.as_str(),
            asset_class_id,
            financial_institution_id,
        };
        let result = crate::asset::ops::create(&self.db_connection, &insertable)?;

        match result {
            Some(asset) => Ok(ReceiveEvent::Message(format!(
                "Created asset '{}'",
                asset.display_name
            ))),
            None => Err(Error::AlreadyExists(display_name)),
        }
    }

    fn load_assets(&self) -> Result {
        let result = crate::asset::ops::load_all(&self.db_connection)?;

        Ok(ReceiveEvent::AssetLoaded(result))
    }

    fn send_event(&self, evt: ReceiveEvent) {
        self.response_channel.send(evt).unwrap();
    }
}

pub struct BackendChannel {
    sender: Mutex<Sender<SendEvent>>,
    receiver: Mutex<Receiver<ReceiveEvent>>,
}

impl BackendChannel {
    pub fn new(sender: Sender<SendEvent>, receiver: Receiver<ReceiveEvent>) -> BackendChannel {
        BackendChannel {
            sender: Mutex::new(sender),
            receiver: Mutex::new(receiver),
        }
    }

    pub fn with_sender<F>(&self, f: F)
    where
        F: FnOnce(&Sender<SendEvent>),
    {
        let sender = self.sender.lock().unwrap();
        f(&sender);
    }

    pub fn with_receiver<F>(&self, f: F)
    where
        F: FnOnce(&Receiver<ReceiveEvent>),
    {
        let receiver = self.receiver.lock().unwrap();
        f(&receiver);
    }
}
