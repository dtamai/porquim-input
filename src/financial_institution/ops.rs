use diesel::prelude::*;

use super::*;
use crate::models::Error;

pub fn create(
    conn: &PgConnection,
    source: &NewFinancialInstitution,
) -> Result<Option<FinancialInstitution>, Error> {
    use crate::schema::financial_institutions;

    let validated = source.validate()?;

    let result = diesel::insert_into(financial_institutions::table)
        .values(validated.model)
        .on_conflict_do_nothing()
        .get_result::<FinancialInstitution>(conn)
        .optional();

    match result {
        Ok(r) => Ok(r),
        Err(error) => Err(Error::DbError(format!("{}", error))),
    }
}

pub fn load_all(conn: &PgConnection) -> Result<Vec<FinancialInstitution>, Error> {
    use crate::schema::financial_institutions;

    match financial_institutions::table
        .order(financial_institutions::display_name.asc())
        .load::<FinancialInstitution>(conn)
    {
        Ok(result) => Ok(result),
        Err(error) => Err(Error::DbError(format!("{}", error))),
    }
}
