pub mod ops;

use crate::models::*;
use crate::schema::*;

use chrono::NaiveDateTime;
use diesel::{Insertable, Queryable};

#[derive(Debug, Queryable)]
pub struct FinancialInstitution {
    pub id: i32,
    pub display_name: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "financial_institutions"]
pub struct NewFinancialInstitution<'a> {
    pub display_name: &'a str,
}

impl<'a> Validation for NewFinancialInstitution<'a> {
    fn validate(&self) -> Result<ValidatedModel<&NewFinancialInstitution<'a>>, Error> {
        if !self.display_name.is_empty() {
            Ok(ValidatedModel { model: self })
        } else {
            Err(Error::Invalid("Give it a name!".to_string()))
        }
    }
}
