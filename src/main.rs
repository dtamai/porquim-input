#![allow(proc_macro_derive_resolution_fallback)]

mod asset;
mod asset_class;
mod backend;
mod db;
mod financial_institution;
mod models;
mod schema;

use crate::asset::Asset;
use crate::asset_class::AssetClass;
use crate::backend::{Backend, BackendChannel, ReceiveEvent, SendEvent};
use crate::financial_institution::FinancialInstitution;

use chrono;
use cursive;
#[macro_use]
extern crate diesel;

use lazy_static::lazy_static;

use cursive::align::*;
use cursive::event::*;
use cursive::theme::{ColorStyle, Effect, Style};
use cursive::traits::*;
use cursive::views::*;
use cursive::Cursive;

use std::sync::mpsc::*;
use std::sync::Mutex;
use std::thread;

#[derive(Default)]
struct State {
    pub asset_class_list: Mutex<Vec<AssetClass>>,
    pub financial_institution_list: Mutex<Vec<FinancialInstitution>>,
    pub asset_list: Mutex<Vec<Asset>>,
}

static ID_FIELD_DISPLAY_NAME: &str = "display_name";
static ID_FIELD_SELECT_ASSET_CLASS: &str = "asset_class";
static ID_FIELD_SELECT_FIN_INST: &str = "financial_institution";
static ID_TEXT_VIEW_BAR: &str = "status_bar";
static ID_TEXT_VIEW_ASSET_LIST: &str = "asset_list";
static ID_TEXT_VIEW_ASSET_CLASS_LIST: &str = "asset_class_list";
static ID_TEXT_VIEW_FINANCIAL_INSTITUTION_LIST: &str = "financial_institution_list";

lazy_static! {
    static ref BACKEND_CHANNEL: BackendChannel = {
        let (event_sender, event_receiver) = channel();
        let (response_sender, response_receiver) = channel();

        let response_sender = Mutex::new(response_sender);
        let event_receiver = Mutex::new(event_receiver);
        thread::spawn(move || {
            // No other thread can access the channels because they are defined
            // in this scope, than they move to the event handler thread.
            // So it is safe to drop the mutexes.
            let rx = event_receiver.into_inner().unwrap();
            let tx = response_sender.into_inner().unwrap();

            Backend::new(rx, tx).handle_events();
        });

        BackendChannel::new(event_sender, response_receiver)
    };

    static ref STATE: State = { State::default() };
}

fn main() {
    init_backend();
    load_state();

    let mut siv = Cursive::default();
    listen_backend_responses(&siv);

    let screen = siv.screen_mut();
    screen.add_transparent_layer(layer_status_bar());
    screen.add_layer(layer_home());

    siv.set_fps(10);
    siv.run();
}

fn init_backend() {
    BACKEND_CHANNEL.with_sender(|sender| sender.send(backend::SendEvent::Ping {}).unwrap());
}

fn load_state() {
    BACKEND_CHANNEL.with_sender(|sender| {
        sender.send(backend::SendEvent::AssetClassLoad).unwrap();
        sender
            .send(backend::SendEvent::FinancialInstitutionLoad)
            .unwrap();
        sender.send(backend::SendEvent::AssetLoad).unwrap();
    });
}

fn listen_backend_responses(siv: &Cursive) {
    let sender = siv.cb_sink().clone();

    thread::spawn(move || {
        BACKEND_CHANNEL.with_receiver(|receiver| {
            for evt in receiver.iter() {
                match evt {
                    ReceiveEvent::Pong => (),

                    ReceiveEvent::Message(msg) => sender
                        .send(Box::new(move |s: &mut Cursive| {
                            let mut view: ViewRef<TextView> = s.find_id(ID_TEXT_VIEW_BAR).unwrap();
                            view.set_content(msg)
                        }))
                        .unwrap(),

                    ReceiveEvent::ErrorMessage(msg) => sender
                        .send(Box::new(move |s: &mut Cursive| {
                            let mut style: Style = ColorStyle::highlight().into();
                            style.effects.insert(Effect::Bold);
                            let text = cursive::utils::span::SpannedString::styled(
                                format!("!!! {} !!!", msg),
                                style,
                            );

                            let mut view: ViewRef<TextView> = s.find_id(ID_TEXT_VIEW_BAR).unwrap();
                            view.set_content(text)
                        }))
                        .unwrap(),

                    ReceiveEvent::AssetClassLoaded(list) => {
                        let mut m_list = STATE.asset_class_list.lock().unwrap();
                        m_list.clear();
                        m_list.extend(list);

                        sender
                            .send(Box::new(move |s: &mut Cursive| {
                                let view: Option<ViewRef<TextView>> =
                                    s.find_id(ID_TEXT_VIEW_ASSET_CLASS_LIST);
                                if view.is_some() {
                                    view.unwrap().set_content(asset_class_list_content())
                                };
                            }))
                            .unwrap()
                    }

                    ReceiveEvent::FinancialInsttutionLoaded(list) => {
                        let mut m_list = STATE.financial_institution_list.lock().unwrap();
                        m_list.clear();
                        m_list.extend(list);

                        sender
                            .send(Box::new(move |s: &mut Cursive| {
                                let view: Option<ViewRef<TextView>> =
                                    s.find_id(ID_TEXT_VIEW_FINANCIAL_INSTITUTION_LIST);
                                if view.is_some() {
                                    view.unwrap()
                                        .set_content(financial_institution_list_content())
                                };
                            }))
                            .unwrap()
                    }

                    ReceiveEvent::AssetLoaded(list) => {
                        let mut m_list = STATE.asset_list.lock().unwrap();
                        m_list.clear();
                        m_list.extend(list);

                        sender
                            .send(Box::new(move |s: &mut Cursive| {
                                let view: Option<ViewRef<TextView>> =
                                    s.find_id(ID_TEXT_VIEW_ASSET_LIST);
                                if view.is_some() {
                                    view.unwrap().set_content(asset_list_content())
                                };
                            }))
                            .unwrap()
                    }
                }
            }
        })
    });
}

fn layer_status_bar() -> impl View {
    let mut layer = LinearLayout::vertical();
    layer.add_child(BoxView::with_full_height(DummyView {}));
    layer.add_child(BoxView::with_full_width(Layer::new(
        TextView::new("Welcome to porquim-input!")
            .v_align(VAlign::Bottom)
            .with_id(ID_TEXT_VIEW_BAR),
    )));
    layer
}

fn layer_home() -> impl View {
    let controls = {
        let items = format!(
            "{a:<30}{q:<30}\n{f:<30}\n{c:<30}",
            a = "[a] Asset",
            q = "[q] Quit",
            c = "[c] Asset class",
            f = "[f] Financial institution",
        );
        OnEventView::new(BoxView::with_min_size(
            (60, 10),
            TextView::new(items).v_align(VAlign::Bottom),
        ))
        .on_pre_event(Event::Char('a'), |s| s.add_layer(panel_asset()))
        .on_pre_event(Event::Char('c'), |s| s.add_layer(panel_asset_class()))
        .on_pre_event(Event::Char('f'), |s| {
            s.add_layer(panel_financial_institution())
        })
        .on_pre_event(Event::Char('q'), |s| {
            s.quit();
        })
    };
    Panel::new(controls).title("porquim-input")
}

fn asset_list_content() -> String {
    let list = STATE.asset_list.lock().unwrap();
    let names = list.iter().fold(String::new(), |acc, item| {
        acc + &format!("\n * {}", item.display_name)
    });

    format!("There are {} assets:\n{}", list.len(), names)
}

fn asset_class_list_content() -> String {
    let list = STATE.asset_class_list.lock().unwrap();
    let names = list.iter().fold(String::new(), |acc, item| {
        acc + &format!("\n * {}", item.display_name)
    });

    format!("There are {} asset classes:\n{}", list.len(), names)
}

fn financial_institution_list_content() -> String {
    let list = STATE.financial_institution_list.lock().unwrap();
    let names = list.iter().fold(String::new(), |acc, item| {
        acc + &format!("\n * {}", item.display_name)
    });

    format!(
        "There are {} financial institutions:\n{}",
        list.len(),
        names
    )
}

fn panel_asset() -> impl View {
    let items = TextView::new(asset_list_content())
        .with_id(ID_TEXT_VIEW_ASSET_LIST)
        .scrollable();
    let control_labels = TextView::new(format!("{n:<20}{q:>20}", n = "[n] New", q = "[q] Close",))
        .v_align(VAlign::Bottom)
        .min_size((80, 18));
    let layout = LinearLayout::vertical()
        .child(items)
        .child(PaddedView::new(((0, 0), (2, 0)), control_labels));
    let controls = OnEventView::new(BoxView::with_min_size((80, 20), layout))
        .on_pre_event(Event::Char('n'), |s| {
            s.add_layer(form_asset());
        })
        .on_pre_event(Event::Char('q'), |s| {
            s.pop_layer();
        });
    Panel::new(controls).title("Asset")
}

fn panel_asset_class() -> impl View {
    let items = TextView::new(asset_class_list_content()).with_id(ID_TEXT_VIEW_ASSET_CLASS_LIST);
    let control_labels = TextView::new(format!("{n:<20}{q:>20}", n = "[n] New", q = "[q] Close",))
        .v_align(VAlign::Bottom)
        .min_size((80, 18));
    let layout = LinearLayout::vertical()
        .child(items)
        .child(PaddedView::new(((0, 0), (2, 0)), control_labels));
    let controls = OnEventView::new(BoxView::with_min_size((80, 20), layout))
        .on_pre_event(Event::Char('n'), |s| {
            s.add_layer(form_asset_class());
        })
        .on_pre_event(Event::Char('q'), |s| {
            s.pop_layer();
        });
    Panel::new(controls).title("Asset Class")
}

fn panel_financial_institution() -> impl View {
    let items = TextView::new(financial_institution_list_content())
        .with_id(ID_TEXT_VIEW_FINANCIAL_INSTITUTION_LIST);
    let control_labels = TextView::new(format!("{n:<20}{q:>20}", n = "[n] New", q = "[q] Close",))
        .v_align(VAlign::Bottom)
        .min_size((80, 18));
    let layout = LinearLayout::vertical()
        .child(items)
        .child(PaddedView::new(((0, 0), (2, 0)), control_labels));
    let controls = OnEventView::new(BoxView::with_min_size((80, 20), layout))
        .on_pre_event(Event::Char('n'), |s| {
            s.add_layer(form_financial_institution());
        })
        .on_pre_event(Event::Char('q'), |s| {
            s.pop_layer();
        });
    Panel::new(controls).title("Financial Institution")
}

fn form_asset() -> impl View {
    let f_inst_select = {
        let mut view = SelectView::new().popup().autojump();
        for i in STATE.financial_institution_list.lock().unwrap().iter() {
            view.add_item(i.display_name.clone(), i.id);
        }
        view.with_id(ID_FIELD_SELECT_FIN_INST)
    };
    let asset_class_select = {
        let mut view = SelectView::new().popup().autojump();
        for i in STATE.asset_class_list.lock().unwrap().iter() {
            view.add_item(i.display_name.clone(), i.id);
        }
        view.with_id(ID_FIELD_SELECT_ASSET_CLASS)
    };
    let fields = ListView::new()
        .child(
            "Display name",
            EditView::new().with_id(ID_FIELD_DISPLAY_NAME).min_width(40),
        )
        .child("Asset class", asset_class_select)
        .child("Financial institution", f_inst_select);

    OnEventView::new(
        Dialog::new()
            .title("New Asset")
            .content(fields)
            .button("Ok", |s| {
                submit_asset(s);
            })
            .dismiss_button("Cancel"),
    )
    .on_event(Key::Esc, |s| {
        s.pop_layer();
    })
}

fn form_asset_class() -> impl View {
    OnEventView::new(
        Dialog::new()
            .title("New Asset Class")
            .content(
                ListView::new().child(
                    "Display name",
                    EditView::new()
                        .on_submit(cb_submit_asset_class)
                        .with_id(ID_FIELD_DISPLAY_NAME)
                        .min_width(40),
                ),
            )
            .button("Ok", |s| {
                let display_name = s
                    .call_on_id(ID_FIELD_DISPLAY_NAME, |view: &mut EditView| {
                        view.get_content()
                    })
                    .unwrap();
                cb_submit_asset_class(s, &display_name);
            })
            .dismiss_button("Cancel"),
    )
    .on_event(Key::Esc, |s| {
        s.pop_layer();
    })
}

fn form_financial_institution() -> impl View {
    OnEventView::new(
        Dialog::new()
            .title("New Financial Institution")
            .content(
                ListView::new().child(
                    "Display name",
                    EditView::new()
                        .on_submit(cb_submit_financial_institution)
                        .with_id(ID_FIELD_DISPLAY_NAME)
                        .min_width(40),
                ),
            )
            .button("Ok", |s| {
                let display_name = s
                    .call_on_id(ID_FIELD_DISPLAY_NAME, |view: &mut EditView| {
                        view.get_content()
                    })
                    .unwrap();
                cb_submit_financial_institution(s, &display_name);
            })
            .dismiss_button("Cancel"),
    )
    .on_event(Key::Esc, |s| {
        s.pop_layer();
    })
}

fn submit_asset(s: &mut Cursive) {
    let display_name = s
        .call_on_id(ID_FIELD_DISPLAY_NAME, |view: &mut EditView| {
            view.get_content()
        })
        .unwrap();
    let asset_class_id: i32 = *s
        .call_on_id(ID_FIELD_SELECT_ASSET_CLASS, |view: &mut SelectView<i32>| {
            view.selection().unwrap()
        })
        .unwrap();
    let financial_institution_id: i32 = *s
        .call_on_id(ID_FIELD_SELECT_FIN_INST, |view: &mut SelectView<i32>| {
            view.selection().unwrap()
        })
        .unwrap();

    let evt = SendEvent::AssetCreate {
        display_name: display_name.to_string(),
        asset_class_id,
        financial_institution_id,
    };

    BACKEND_CHANNEL.with_sender(|sender| {
        sender.send(evt).unwrap();
        sender.send(SendEvent::AssetLoad).unwrap();
    });

    s.pop_layer();
}

fn cb_submit_asset_class(s: &mut Cursive, display_name: &str) {
    let evt = SendEvent::AssetClassCreate {
        display_name: display_name.to_string(),
    };
    BACKEND_CHANNEL.with_sender(|sender| {
        sender.send(evt).unwrap();
        sender.send(SendEvent::AssetClassLoad).unwrap();
    });

    s.pop_layer();
}

fn cb_submit_financial_institution(s: &mut Cursive, display_name: &str) {
    let evt = SendEvent::FinancialInstitutionCreate {
        display_name: display_name.to_string(),
    };
    BACKEND_CHANNEL.with_sender(|sender| {
        sender.send(evt).unwrap();
        sender.send(SendEvent::FinancialInstitutionLoad).unwrap();
    });

    s.pop_layer();
}
