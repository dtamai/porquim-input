use crate::models::Error;
pub use diesel::pg::PgConnection;
use diesel::prelude::*;
use std::env;

pub fn connect_now() -> Result<PgConnection, Error> {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).or_else(|_| {
        Err(Error::DbError(
            "Failed to establish connection to the database".to_string(),
        ))
    })
}
