use diesel::prelude::*;

use super::*;
use crate::models::Error;

pub fn create(conn: &PgConnection, source: &NewAssetClass) -> Result<Option<AssetClass>, Error> {
    use crate::schema::asset_classes;

    let validated = source.validate()?;

    let result = diesel::insert_into(asset_classes::table)
        .values(validated.model)
        .on_conflict_do_nothing()
        .get_result::<AssetClass>(conn)
        .optional();

    match result {
        Ok(r) => Ok(r),
        Err(error) => Err(Error::DbError(format!("{}", error))),
    }
}

pub fn load_all(conn: &PgConnection) -> Result<Vec<AssetClass>, Error> {
    use crate::schema::asset_classes;

    match asset_classes::table
        .order(asset_classes::display_name.asc())
        .load::<AssetClass>(conn)
    {
        Ok(result) => Ok(result),
        Err(error) => Err(Error::DbError(format!("{}", error))),
    }
}
