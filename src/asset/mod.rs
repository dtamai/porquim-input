pub mod ops;

use crate::models::*;
use crate::schema::*;
use chrono::NaiveDateTime;
use diesel::{Insertable, Queryable};

#[derive(Debug, Queryable)]
pub struct Asset {
    pub id: i32,
    pub display_name: String,
    pub asset_class_id: i32,
    pub financial_institution_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "assets"]
pub struct NewAsset<'a> {
    pub display_name: &'a str,
    pub asset_class_id: i32,
    pub financial_institution_id: i32,
}

impl<'a> Validation for NewAsset<'a> {
    fn validate(&self) -> Result<ValidatedModel<&NewAsset<'a>>, Error> {
        if !self.display_name.is_empty() {
            Ok(ValidatedModel { model: self })
        } else {
            Err(Error::Invalid("Give it a name!".to_string()))
        }
    }
}
