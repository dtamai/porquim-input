use diesel::prelude::*;

use super::*;
use crate::models::Error;

pub fn create(conn: &PgConnection, source: &NewAsset) -> Result<Option<Asset>, Error> {
    use crate::schema::assets;

    let validated = source.validate()?;

    let result = diesel::insert_into(assets::table)
        .values(validated.model)
        .on_conflict_do_nothing()
        .get_result::<Asset>(conn)
        .optional();

    match result {
        Ok(r) => Ok(r),
        Err(error) => Err(Error::DbError(format!("{}", error))),
    }
}

pub fn load_all(conn: &PgConnection) -> Result<Vec<Asset>, Error> {
    use crate::schema::assets;

    match assets::table
        .order(assets::display_name.asc())
        .load::<Asset>(conn)
    {
        Ok(result) => Ok(result),
        Err(error) => Err(Error::DbError(format!("{}", error))),
    }
}
