#[derive(Debug)]
pub enum Error {
    AlreadyExists(String),
    DbError(String),
    Invalid(String),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let msg = match self {
            Error::AlreadyExists(name) => format!("Already exists resource '{}'", name),
            Error::DbError(msg) => format!("Database error: '{}'", msg).to_string(),
            Error::Invalid(msg) => msg.to_string(),
        };
        write!(f, "{}", msg)
    }
}

pub struct ValidatedModel<T> {
    pub model: T,
}

pub trait Validation {
    fn validate(&self) -> Result<ValidatedModel<&Self>, Error>;
}
