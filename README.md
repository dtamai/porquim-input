# porquim-input

Interface para input de dados para o Porquim

## Serviços

Serviços estão definidos no _docker-compose.yml_.

O projeto usa PostgreSQL para persistência de dados.

## Migrations

As migrations são gerenciadas pelo [diesel-cli](https://github.com/diesel-rs/diesel/tree/master/diesel_cli). Em resumo: `diesel migration run` pra rodar e `diesel migration redo` pra garantir que o script _down_ funciona.
